package inputstub;

/**
 * input generator entry point
 */
public class Main{

    /**
     * Call the program as follows: program n i j k ...
     * Here n, i, j and k represent the number of items in the xml file to be generated.
     * Specify as many arguments as you would like files generated
     */
    public static void main(String args[]){
        int cnt = 1;
        for(String number : args){
            int n = Integer.parseInt(number);
            new InputFile(n,cnt++);
        }
    }
}