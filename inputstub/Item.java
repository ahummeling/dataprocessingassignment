package inputstub;

public enum Item{
    URL,
    TIME,
    STRING,
    POSTAL,
    LENGTH,
    INTEGER,
    ID
}
