package inputstub;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.File;
import java.util.Random;

/**
 * class for generating input
 */
public class InputFile{

    private FileOutputStream fos;
    private int iteration;
    private Random rand;

    /**
     * constructor for the file generator. 
     * Will construct 1 file with nItems number of items 
     * @param nItems number of items
     * @param iteration the iteration of file
     */
    public InputFile(int nItems, int iteration){
        this.iteration = iteration;
        rand = new Random();
        createFile();
        write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<generatedData>\n");
        for(int i = 1; i<=nItems; i++){
            write("    <item>\n");
            for(Item item : Item.values()){
                write(createString(item));
            }
            write("    </item>\n");
        }
        write("</generatedData>");
        closeFile();
    }

    public String createString(Item item){
        switch(item){
            case URL:
                return randomURL();
            case TIME: 
                return randomTime();
            case STRING:
                return randomString();
            case POSTAL:
                return randomPostal();
            case LENGTH:
                return randomLength();
            case INTEGER:
                return randomInteger();
            case ID:
                return randomID();
            default:
                return null;
        }
    }

    public String randomURL(){
        double r = rand.nextDouble();
        if(r<0.2){
            return null;
        }
        if(r<0.5){
            return "www.invalidurl.rekt";
        }
        else{
            return "https://www.validurl.nl";
        }
    }

    public String randomTime(){
        double r = rand.nextDouble();
        if(r<0.2){
            return null;
        }
        if(r<0.5){
            return "460827252";
        }
        else{
            return "1995-03-01";
        }
    }

    public String randomString(){
        double r = rand.nextDouble();
        if(r<0.2){
            return null;
        }
        if(r<0.5){
            //TODO: find something that cannot be printed as string to return
            return "idk";
        }
        else{
            return "this is a string :)";
        }
    }

    public String randomPostal(){
        double r = rand.nextDouble();
        if(r<0.2){
            return null;
        }
        if(r<0.5){
            return "GL 9725";
        }
        else{
            return "9725 GL";
        }
    }

    public String randomLength(){
        double r = rand.nextDouble();
        if(r<0.2){
            return null;
        }
        if(r<0.5){
            return "42";
        }
        else{
            return "38";
        }
    }

    public String randomInteger(){
        double r = rand.nextDouble();
        if(r<0.2){
            return null;
        }
        if(r<0.5){
            return "3.0001";
        }
        else{
            return "3.0000";
        }
    }

    public String randomID(){
        return null;
    }

    /**
     * instantiates the file outputstream to fos
     */
    public void createFile(){
        try{
            new File("XmlFiles").mkdir();
            String filename = "XmlFiles/file"+iteration+".xml";
            fos = new FileOutputStream(filename);
        }
        catch (FileNotFoundException e){
            System.out.println("File not found");
            e.printStackTrace();
        }
    }

    /**
     * writes to the fileoutputstream defined in the class
     * @param line line to write to file
     */
    public void write(String line){
        try{
            byte[] bytesArray = line.getBytes();
            fos.write(bytesArray);
        }
        catch (IOException e){
            e.printStackTrace();
        }

    }

    /**
     * closes the fileoutputstream
     */
    public void closeFile(){
        try{
            fos.close();
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }

}