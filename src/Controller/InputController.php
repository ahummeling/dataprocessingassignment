<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use DateTime;

class InputController extends AbstractController
{
    /**
      * @Route("getInput")
      */
    public function getInput() : Response
    {
        $foundFile = "Success!";
        //attempts to load the .xml file into a variable
        try{
            $xmlClass = simplexml_load_file ("../XmlFiles/file1.xml");
        } catch (FileNotFoundException $e){
            $e.printStackTrace();
            $foundFile = "Failed";
        }
        $grades = $this->gradeXml($xmlClass);
        return $this->render('inputcontroller/getInput.html.twig', [
            'foundFile' => $foundFile, 'grades' => $grades
        ]);
    }

    /**
     * loops over all items to grade each string in the item
     * @param xmlFile an xml file to grade
     */
    public function gradeXml($xmlFile) : String 
    {
        $grades = array();
        $cnt = 0;
        foreach($xmlFile->item as $item){
            $cnt++;
            array_push($grades, " Item $cnt: ");
            array_push($grades, $this->isValidUrl( $item->string0),
            $this->isValidTime( $item->string1),
            $this->isPrintableString( $item->string2),
            $this->isPostalCode( $item->string3), 
            $this->isCorrectLength( $item->string4), 
            $this->isInteger( $item->string5), 
            $this->isID( $item->string6));
        }
        return implode($grades);
    }

    /**
     * returns a grade based on the validity of the input as a url
     * @param string0 string to grade
     */
    public function isValidUrl($string0) : String
    {
        //note that https://www.w3.org/Addressing/URL/url-spec.txt was used to define what a valid url is
        if($string0 == null){
            return 'F';
        }
        if(!(substr($string0,0,7) == "http://" && substr($string0,0,8) == "https://")) {
            return 'D';
        }
        if( $string0[4] == 's' ){
            $https = true;
        } else{
            $https = false;
        }
        return 'A';
        //TODO: write the rest of the URL validation.
        //Alternative idea: Just try to query the DNS server with the string to see if it outputs an adress.. 
    }

    /**
     * returns grading based on the validity of the string as a DateTime object
     * @param string1 string to grade
     */
    public function isValidTime($string1)
    {
        if($string1 == null){
            return 'F';
        }
        if(strtotime($string1) === false){
            return 'C';
        } else{
            return 'A';
        }
    }

    /**
     * returns grading based on whether the string is printable or not
     * @param string2 string to grade
     */
    public function isPrintableString($string2)
    {
        if($string2 == null || !mb_detect_encoding($string2, 'ASCII', true)){
            return 'B';
        } else{
            return 'A';
        }
    }

    /**
     * checks whether the string has a valid postalcode, and grades accordingly
     * @param string3 string to grade
     */
    public function isPostalCode($string3){
        if($string3 == null){
            return 'F';
        }
        if(!preg_match('^[1-9][0-9]{3}\s?([a-zA-Z]{2})?^',$string3)){
            return 'C';
        }
        return 'A';
    }

    /**
     * checks whether the param has the correct length
     * @param string4 string to grade
     */
    public function isCorrectLength($string4){
        if($string4 == null || strlen($string4) < 5 || strlen($string4)>40){
            return 'B';
        }
        return 'A';
    }

    /**
     * checks whether the string works as a valid int without data loss
     * @param string5 string to grade
     */
    public function isInteger($string5){
        if($string5 == null){
            return 'B';
        }
        $number = floatval($string5);
        $intNumber = (int) $number;
        if($number != 0 && (float) $intNumber - $number < 0 ){
            return 'B';
        } else{
            return 'A';
        }
    }

    /**
     * checks uniqueness of ID
     */
    public function isID($string6)
    {
        return 'A';
        //TODO: write a static method to keep track of ID's that have come along and find an efficient method
        //to check for uniqueness
    }

}
?>